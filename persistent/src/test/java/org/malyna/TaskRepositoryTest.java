package org.malyna;

import autofixture.publicinterface.Any;
import org.junit.After;
import org.junit.runner.RunWith;
import org.malyna.commons.entities.Task;
import org.malyna.persistence.configurations.TestEmbeddedDatabaseConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {TestEmbeddedDatabaseConfiguration.class})
@ActiveProfiles("test")
public class TaskRepositoryTest extends AbstractTestNGSpringContextTests {

	@Autowired
	private CrudRepository<Task, String> repository;

	@After
	public void tearDown() throws Exception {
		repository.deleteAll();
	}

	@Test(invocationCount = 10)
	public void shouldAddTaskProperly() throws Exception {
		//GIVEN
		Task task = Any.anonymous(Task.class);

		//WHEN
		Task result = repository.save(task);

		//THEN
		assertThat(result).isEqualToComparingFieldByField(task);
	}

	@Test
	public void shouldRemoveTaskProperly() throws Exception {
		//GIVEN
		Task task = Any.anonymous(Task.class);
		repository.save(task);

		//WHEN
		repository.deleteById(task.getId());

		//THEN
		Optional<Task> result = repository.findById(task.getId());
		assertThat(result).isEmpty();
	}

	@Test
	public void shouldGetElementProperly() throws Exception {
		//GIVEN
		Task task = Any.anonymous(Task.class);
		repository.save(task);

		//WHEN
		Optional<Task> result = repository.findById(task.getId());

		//THEN
		assertThat(result).isNotEmpty();
		assertThat(result.get()).isEqualToComparingFieldByField(task);
	}
}
