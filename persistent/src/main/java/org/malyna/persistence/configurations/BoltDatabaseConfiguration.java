package org.malyna.persistence.configurations;


import org.neo4j.ogm.session.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.transaction.Neo4jTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan("org.malyna.pesrsistence")
@EnableNeo4jRepositories("org.malyna.persistence.repositories")
@Profile("production")
public class BoltDatabaseConfiguration {
	@Bean(name = "sessionFactory")
	public SessionFactory getSessionFactory() {
		return  new SessionFactory(configuration(), "org.malyna.commons.entities");
	}

	@Bean
	public Neo4jTransactionManager transactionManager() throws Exception {
		return new Neo4jTransactionManager(getSessionFactory());
	}

	@Bean
	public org.neo4j.ogm.config.Configuration configuration() {
		return new org.neo4j.ogm.config.Configuration.Builder()
			.uri("bolt://localhost")
			.credentials("neo4j", "admin1")
			.build();
	}
}

