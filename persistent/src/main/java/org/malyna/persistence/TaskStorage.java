package org.malyna.persistence;

import org.malyna.commons.data.persistent.interfaces.DataAccessObject;
import org.malyna.commons.entities.Task;
import org.malyna.persistence.repositories.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TaskStorage implements DataAccessObject<Task> {
	@Autowired
	private TaskRepository repository;

	@Override
	public Optional<Task> save(Task task) {
		return Optional.of(repository.save(task));
	}

	@Override
	public List<Task> bulkGet(String login) {
		return repository.getAllUsersTask(login);
	}

	@Override
	public void remove(String id) {
		repository.deleteById(id);
	}

	@Override
	public Optional<Task> get(String id) {
		return repository.findById(id);
	}

}
