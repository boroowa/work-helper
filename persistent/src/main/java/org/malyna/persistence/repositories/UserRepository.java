package org.malyna.persistence.repositories;

import org.malyna.commons.entities.User;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

public interface UserRepository extends Neo4jRepository<User, String> {
}
