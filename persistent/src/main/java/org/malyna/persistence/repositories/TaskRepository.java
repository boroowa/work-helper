package org.malyna.persistence.repositories;

import org.malyna.commons.entities.Task;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Qualifier("neo4j")
public interface TaskRepository extends CrudRepository<Task, String> {
	@Query("MATCH (u:USER {id:{0}})-[:HAS]->(task:Task)")
	List<Task> getAllUsersTask(String username);
}
