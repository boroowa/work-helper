import org.malyna.tests.acceptance.TaskSuite

object Main {
  def main(args: Array[String]) : Unit = {
    val test : TaskSuite = new TaskSuite()
    test.runTaskTests()
  }
}