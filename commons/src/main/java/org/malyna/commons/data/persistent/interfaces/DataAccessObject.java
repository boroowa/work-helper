package org.malyna.commons.data.persistent.interfaces;

import java.util.List;
import java.util.Optional;

public interface DataAccessObject<T0> {
	Optional<T0> save(T0 task);
	List<T0> bulkGet(String login);
	void remove(String id);
	Optional<T0> get(String id);
}