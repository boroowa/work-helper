package org.malyna.commons.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@NodeEntity
@ToString
@Builder(toBuilder = true)
@Data
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Task {
    @Id
    @NotNull @Size(min = 1)
	private String id;
	@NotNull @Size(min = 2, max = 256) @NotEmpty
    private String name;
	@NotNull
    private String progressColumn;
    private String description;
    @NotNull @Positive
    private Long creationDateEpoch;
    private Long priority;
    private String type;
    @NotNull @Size(min = 1, max = 256)
    private String creator;
}
